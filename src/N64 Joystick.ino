#include <Arduino.h>

// -----------------------------------------------------------------
// ATtiny24A Chip Pinout
// -----------------------------------------------------------------
// Chip is mounted upside-down on joystick board on middle pins!!
//
//                              +-\/-+
//                         VCC 1|    |14 GND
//                         PB0 2|    |13 PA0/ADC0 - X Pot
//                         PB1 3|    |12 PA1/ADC1
//              (grey) RES/PB3 4|    |11 PA2/ADC2 - Y Pot
//                    XB - PB2 5|    |10 PA3/ADC3 - YB
//               XA - ADC7/PA7 6|    |9  PA4/ADC4/SCL - YA (white)
//      (purple) MOSI/ADC6/PA6 7|    |8  PA5/ADC5/MISO (black)
//                              +----+
// -----------------------------------------------------------------

#define XANAIN  0  // ADC0
#define YANAIN  2  // ADC2

#define XA      3  // PA7
#define XB      2  // PB2
#define YA      6  // PA4
#define YB      7  // PA3

// Affects range of X and Y axis (1024 * R / 128)
// R = 20 means the range is 160 steps (default)
// R = 21 means the range is 168 steps
#define RANGEMOD 20

// Emulates encoder wheels of N64 stick, only bottom 2 bits of each are used
byte xWheel = 0xCC;  // 11001100
byte yWheel = 0xCC;  // 11001100

int xCur, yCur;
int xSteps, xOld;
int ySteps, yOld;

void setup()
{  
  //Serial.begin(115200);
  
  pinMode(XA, OUTPUT);
  pinMode(XB, OUTPUT);
  pinMode(YA, OUTPUT);
  pinMode(YB, OUTPUT);
  
  // All outputs initially high
  digitalWrite(XA, HIGH);
  digitalWrite(XB, HIGH);
  digitalWrite(YA, HIGH);
  digitalWrite(YB, HIGH);
  
  // Take initial ADC reading of X and Y position
  xCur = analogRead(XANAIN);
  yCur = analogRead(YANAIN);

  // Convert ADC readings to appropriate range (0-159)
  xCur *= RANGEMOD;
  xCur >>= 7;
  
  yCur *= RANGEMOD;
  yCur >>= 7;
    
  xOld = xCur;
  yOld = yCur;
}

void loop()
{
  // Take ADC reading of X and Y position
  xCur = analogRead(XANAIN);
  yCur = analogRead(YANAIN);

  // Convert ADC readings to appropriate range (0-159)
  xCur *= RANGEMOD;
  xCur >>= 7;
  
  yCur *= RANGEMOD;
  yCur >>= 7;
  
  //Serial.print("X:");
  //Serial.print(xCur);  
  //Serial.print("\tY:");
  //Serial.println(yCur);
  
  // Find out the step difference from previous value
  xSteps = xCur - xOld;
  ySteps = yCur - yOld;

  // If we have any steps to take for X or Y...
  while (xSteps != 0 || ySteps != 0)
  {
    if (xSteps > 0)
    {
      // Rotate xWheel left 1 bit
      xWheel = ((xWheel & 0x80) ? 0x01 : 0x00) | (xWheel << 1);
      xSteps -= 1;    
    }
    else if (xSteps < 0)
    {
      // Rotate xWheel right 1 bit
      xWheel = ((xWheel & 0x01) ? 0x80 : 0x00) | (xWheel >> 1);
      xSteps += 1;
    }
    
    if (ySteps > 0)
    {
      // Rotate yWheel left 1 bit
      yWheel = ((yWheel & 0x80) ? 0x01 : 0x00) | (yWheel << 1);
      ySteps -= 1;    
    }
    else if (ySteps < 0)
    {
      // Rotate yWheel right 1 bit
      yWheel = ((yWheel & 0x01) ? 0x80 : 0x00) | (yWheel >> 1);
      ySteps += 1;
    }
    
    // Output grey code step (lowest 2 bits of xWheel and yWheel)
    digitalWrite(XA, (xWheel & 0x2 ? 1 : 0));
    digitalWrite(XB, (xWheel & 0x1 ? 1 : 0));
    digitalWrite(YA, (yWheel & 0x1 ? 1 : 0));
    digitalWrite(YB, (yWheel & 0x2 ? 1 : 0));
    
    // Little delay otherwise the N64 IC may skip some steps
    delayMicroseconds(10);
  }
  
  // Save values for difference next time around
  xOld = xCur;
  yOld = yCur;
}
